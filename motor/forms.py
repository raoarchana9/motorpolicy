from django import forms
from cProfile import label
from django.core.validators import RegexValidator

import re
from django.core.exceptions import ValidationError

def test(val):
    if re.match(r'^[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}$', val):
        raise ValidationError('Disallowed Tags')
    
class MotorPolicy(forms.Form):
    vehicleNumber = forms.CharField(label= "Vehicle Number")
    choices = (('1', 'Yes'), ('2', 'No'))
    loanType = forms.CharField(label = "Do you have a policy", widget=forms.Select(choices=choices)) 
