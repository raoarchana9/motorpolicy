# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from commonlib.helpers.logger import Logger
from commonlib.helpers.general_helper import Helper
from forms import MotorPolicy
from django.http  import JsonResponse, HttpResponse

logger = Logger()
helper = Helper()

"""
"""
from django.contrib.auth.decorators import login_required
@login_required(login_url='/account/login/')
def homePage(req):
    try:
        loggerObj = logger.getLogger("motor_home")
        reqParams = helper.processRequest(req)
        if(req.method == "POST"):
            import re
            pattern = r'^[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}$'
            if (re.match(pattern , reqParams['vehicleNumber']) is None):
                return JsonResponse({'status' : 'failure', 'errCode': 400, 
                                         'description' : "Invalid format for vehicle number. Eg: MH03CB8906"}, safe=False)
            else:
                return JsonResponse({'status' : 'success', 'errCode': 0, 
                                             'description' : "Successfully Registered"}, safe=False)
        else:
            loanForm = MotorPolicy()
        return render(req, 'motor/home_page.html', {'form' : loanForm})
    except Exception as e:
        import sys
        loggerObj.warn("Exception while rendering home page " + str(e) 
                    +  "Error on line {}" .format(sys.exc_info()[-1].tb_lineno))
        return JsonResponse({'status' : 'failure', 'errCode': 500, 'errMsg' : str(e)}, safe=False)

    
