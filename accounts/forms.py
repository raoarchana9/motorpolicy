from django import forms

"""
Model used in case of registration
"""
class UserRegistrationForm(forms.Form):
    phone = forms.CharField(
        required = True,
        label = 'Phone',
        max_length = 32
    )
    password = forms.CharField(
        required = True,
        label = 'Password',
        max_length = 32,
        widget = forms.PasswordInput()
    )