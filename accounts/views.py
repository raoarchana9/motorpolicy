# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from forms import UserRegistrationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.http  import JsonResponse, HttpResponse

from django import forms


"""
Used to register a user using phone and password
"""
def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            userObj = form.cleaned_data
            username = userObj['phone']
            password =  userObj['password']
            if not (User.objects.filter(username=username).exists()):
                User.objects.create_user(username, None, password)
                user = authenticate(username = username, password = password)
                login(request, user)
                return HttpResponseRedirect('/account/login')
            else:
                return JsonResponse({'status' : 'failure', 'errCode': 500, 
                                             'description' : "User alrerady exists"}, safe=False)
    else:
        form = UserRegistrationForm()

    return render(request, 'accounts/register.html', {'form' : form})