

class Helper:    
    '''
    returns: dictionary
    params: request Object
    returns the parameters of the request object in dictionary 
    form
    '''       
    def processRequest(self, reqObj):
        if(reqObj.method == "GET"):
            return reqObj.GET.dict()
        if(reqObj.method == "POST"):
            return reqObj.POST.dict()

        
