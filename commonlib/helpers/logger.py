import logging
from datetime import date

class Logger:
    
    def getLogger(self, fileName):
        logger = logging.getLogger(fileName + "_")
        if not logger.handlers:
            fileHandler = logging.FileHandler('/tmp/' + fileName + '_' + str(date.today()) + '.log')
            formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            fileHandler.setFormatter(formatter)
            logger.addHandler(fileHandler)
            logger.setLevel(logging.DEBUG)
        return logger
